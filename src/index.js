require('dotenv').config();
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();

const {notesRouter} = require('./controllers/notesController');
const {authRouter} = require('./controllers/authController'); 
const {usersRouter} = require('./controllers/usersController'); 
const {authMiddleware} = require('./middlewares/authMiddleware'); 

const PORT = process.env.PORT;
const DB_URI = process.env.DB_URI;
const dbOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
};

app.use(morgan('short'));
app.use(cors());
app.use(express.json());

app.use('/api/auth', authRouter);

app.use(authMiddleware);
app.use('/api/notes', notesRouter);
app.use('/api/users', usersRouter);

app.use((err, req, res, next) => {
    res.status(err.status ? err.status : 500).json({message: err.message});
});

const start = async () => {
    try {
        await mongoose.connect(DB_URI, dbOptions);
    
        app.listen(PORT, () => {
            console.log(`Server is listening on port ${PORT}`);
        });

    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();