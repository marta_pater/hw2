const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');

const registration = async ({username, password}) => {
    const user = new User({
        username,
        password: await bcrypt.hash(password, 10)
    });
    await user.save();
}

const login = async ({username, password}) => {
    const user = await User.findOne({username});

    if (!user) {
        throw new Error('string');
    }

    if (!(await bcrypt.compare(password, user.password))) {
        throw new Error('string');
    }

    const jwt_token = jwt.sign({
        _id: user._id,
        username: user.username
    }, 'secret');
    return jwt_token;
}

module.exports = {
    registration,
    login
};