const {User} = require('../models/userModel');

const getUserByUserId = async (userId) => {
    const user = await User.findById({_id: userId});

    if (!user) {
        throw new Error('No user with such id found!');
    }

    return user;
}

const deleteUserByUserId = async (userId) => {
    await User.findOneAndRemove({_id: userId});
}

const changePasswordForUser = async (userId, oldPassword, newPassword) => {
    await User.updateOne({_id: userId}, {password: newPassword});
}

module.exports = {
    getUserByUserId,
    deleteUserByUserId,
    changePasswordForUser
};