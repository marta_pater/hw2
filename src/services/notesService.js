const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId, offset = 0, limit = 0) => {
    const notes = await Note.find({userId}, {
        limit,
        skip: offset
    });
    return notes;
}

const geAlltNotesCountByUserId = async (userId) => {
    return Note.count({userId});
}

const getNoteByIdForUser = async (noteId, userId) => {
    const note = await Note.findOne({_id: noteId, userId});
    return note;
}

const addNoteToUser = async (userId, notePayload) => {
    const note = new Note({...notePayload, userId});
    await note.save(); 
}

const changeCompleted = async (noteId, userId) => {
    const note = await Note.find({_id: noteId, userId});
    const updateCopmleted = !note.completed;
    return Note.updateOne({_id: noteId, userId}, {completed: updateCopmleted});
}

const updateNoteByIdForUser = async (noteId, userId, data) => {
    await Note.findOneAndUpdate({_id: noteId, userId}, { $set: data});
}

const deleteNoteByIdForUser = async (noteId, userId) => {
    await Note.findOneAndRemove({_id: noteId, userId});
}

module.exports = {
    getNotesByUserId,
    geAlltNotesCountByUserId,
    addNoteToUser,
    getNoteByIdForUser,
    updateNoteByIdForUser,
    deleteNoteByIdForUser,
    changeCompleted
};