const express = require('express');
const router = express.Router();

const {
    getUserByUserId,
    deleteUserByUserId,
    changePasswordForUser
} = require('../services/usersService');

router.get('/me', async (req, res) => {
    const { userId } = req.user;

    const user = await getUserByUserId(userId);

    res.status(200).json({user});
});

router.delete('/me', async (req, res) => {
    const { userId } = req.user;

    await deleteUserByUserId(userId);

    res.status(200).json({message: "Success"});
});

router.patch('/me', async (req, res) => {
    const { userId } = req.user;
    const { oldPassword } = req.body.oldPassword;
    const { newPassword } = req.body.newPassword;

    await changePasswordForUser(userId, oldPassword, newPassword);

    res.status(200).json({message: "Success"});
});

module.exports = {
    usersRouter: router
}