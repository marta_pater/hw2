const express = require('express');
const router = express.Router();

const {
    getNotesByUserId,
    geAlltNotesCountByUserId,
    addNoteToUser,
    getNoteByIdForUser,
    updateNoteByIdForUser,
    deleteNoteByIdForUser,
    changeCompleted
} = require('../services/notesService');

router.get('/', async (req, res) => {
    const { offset, limit } = req.query;
    const { userId } = req.user;

    const notes = await getNotesByUserId(userId, offset, limit);
    const notesCount = await geAlltNotesCountByUserId(userId);

    res.status(200).json({
        offset: offset,
        limit: limit,
        count: notesCount,
        notes
    });
});

router.get('/:id', async (req, res) => {
    try{
    const { userId } = req.user;
    const { id } = req.params;

    const note = await getNoteByIdForUser(id, userId);

    if (!note) {
        throw new Error('No note with such id found!');
    }

    res.status(200).json({note});
    } catch(err) {
        res.status(500).json({message: err.message});
    }
});

router.post('/', async (req, res) => {
    try{
    const { userId } = req.user;

    await addNoteToUser(userId, req.body);

    res.status(200).json({message: "Success"});
    } catch {
        res.status(500).json({message: err.message});
    }
});

router.put('/:id', async (req, res) => {
    try{
    const { userId } = req.user;
    const { id } = req.params;
    const data = req.body;

    await updateNoteByIdForUser(id, userId, data);

    res.status(200).json({message: "Success"});
    } catch(err) {
        res.status(500).json({message: err.message});
    }
});

router.patch('/:id', async (req, res) => {
    try{
    const { userId } = req.user;

    await changeCompleted(req.params.id, userId);

    res.status(200).json({message: "Success"});
    } catch(err) {
        res.status(500).json({message: err.message});
    }
});

router.delete('/:id', async (req, res) => {
    try{
    const { userId } = req.user;
    const { id } = req.params;

    await deleteNoteByIdForUser(id, userId);

    res.status(200).json({message: "Success"});
    } catch(err) {
        res.status(500).json({message: err.message});
    }
});

module.exports = {
    notesRouter: router
}