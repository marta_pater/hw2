const express = require('express');
const router = express.Router();

const {
    registration,
    login
} = require('../services/authService');

router.post('/register', async (req, res) => {
    try {
        const {
            username,
            password
        } = req.body;

        await registration({username, password});

        res.status(200).json({message: 'Success'});
    } catch (err) {
        res.status(500).json({message: err.message});
    }
});

router.post('/login', async (req, res) => {
    try {
        const {
            username,
            password
        } = req.body;

        const jwt_token = await login({username, password});

        res.status(200).json({message: 'Success', jwt_token});
    } catch (err) {
        res.status(500).json({message: err.message});
    }
});

module.exports = {
    authRouter: router
}